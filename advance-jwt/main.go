package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Message struct {
	Status	string	`json:"status"`
	Info	string	`json:"info"`
}

func main() {
	http.HandleFunc("/home", VerifyJWT(handlePage))
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Println("There was an error listenting on port :8080", err)
	}
}

func handlePage(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var message Message

	err := json.NewDecoder(request.Body).Decode(&message)
	if err != nil {
		return
	}

	err = json.NewEncoder(writer).Encode(message)
	if err != nil {
		return
	}
}