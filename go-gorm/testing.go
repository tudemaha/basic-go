package main

import "gorm.io/gorm"

// has one
type Animal struct {
	gorm.Model
	CreditCard CreditCard
}

type CreditCard struct {
	gorm.Model
	Number   string
	AnimalID uint
}

// belongs to
type Dontol struct {
	gorm.Model
	Name      string
	CompanyID int
	Company   Company
}

type Company struct {
	ID   int
	Name string
}

func Testing() {
	db := OpenSQL()
	db.AutoMigrate(&Animal{}, &CreditCard{})
	db.AutoMigrate(&Dontol{}, &Company{})
}
