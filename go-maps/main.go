package main

import (
	"context"
	"log"

	"github.com/kr/pretty"
	"googlemaps.github.io/maps"
)

func main() {
	c, err := maps.NewClient(maps.WithAPIKey("AIzaSyDKfSsbRqbv1_JuGCyn6Yuh7SoTEr8IpjQ"))
	if err != nil {
		log.Fatalf("%s", err)
	}

	address := "Kampus Udayana Sudirman"

	r := &maps.GeocodingRequest{
		Address: address,
	}

	geocoding, err := c.Geocode(context.Background(), r)
	if err != nil {
		log.Fatalf("%s", err)
	}

	// for _, geo := range geocoding {
	// 	for _, ac := range geo.AddressComponents {
	// 		if ac.Types[0] == "administrative_area_level_1" {
	// 			fmt.Println(address)
	// 			fmt.Println(ac.ShortName)
	// 		}
	// 	}
	// }

	pretty.Println(geocoding)
}
