package main

import (
	"fmt"
	"log"
	"net/smtp"
	"strings"
)

const (
	CONFIG_SMTP_HOST     = "smtp.gmail.com"
	CONFIG_SMTP_PORT     = 587
	CONFIG_SENDER_NAME   = "Dummy Sender"
	CONFIG_AUTH_EMAIL    = ""
	CONFIG_AUTH_PASSWORD = ""
)

func main() {
	to := []string{""}
	subject := "Testing Mail"
	message := "Hi bro!"

	err := sendMail(to, subject, message)
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println("Mail sent!")
}

func sendMail(to []string, subject, message string) error {
	body := "From: " + CONFIG_SENDER_NAME + "\n" +
		"To: " + strings.Join(to, ",") + "\n" +
		"Subject: " + subject + "\n\n" +
		message

	auth := smtp.PlainAuth("", CONFIG_AUTH_EMAIL, CONFIG_AUTH_PASSWORD, CONFIG_SMTP_HOST)
	smtpAddr := fmt.Sprintf("%s:%d", CONFIG_SMTP_HOST, CONFIG_SMTP_PORT)

	err := smtp.SendMail(smtpAddr, auth, CONFIG_AUTH_EMAIL, to, []byte(body))
	if err != nil {
		return err
	}

	return nil
}
