package main

import (
	"encoding/csv"
	"log"
	"os"
)

func main() {
	f, err := os.Open("data.csv")
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	csvReader := csv.NewReader(f)
	
	// ReadLineCsv(csvReader)
	ReadAllCsv(csvReader)
}