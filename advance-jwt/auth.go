package main

import "net/http"

func AuthPage(writer http.ResponseWriter) {
	token, err := GenerateJWT()
	if err != nil {
		return
	}

	client := &http.Client{}
	request, _ := http.NewRequest("POST", "<http://localhost:8080/>", nil)
	request.Header.Set("Token", token)
	_, _ = client.Do(request)
}