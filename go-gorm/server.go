package main

import (
	"log"
	"net/http"
)

func StartServer() {
	http.HandleFunc("/users", GetAllUsers())
	http.HandleFunc("/user", NewUser())
	http.HandleFunc("/user/", DeleteUser())

	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		log.Fatalf("StartServer fatal error: %v", err)
	}
}
