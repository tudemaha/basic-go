package main

import (
	"go-pagination/controller"
	"log"
	"net/http"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	http.HandleFunc("/", controller.HandleIndex())
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {})

	log.Println("Server started at http://localhost:8080")
	http.ListenAndServe("localhost:8080", nil)
}
