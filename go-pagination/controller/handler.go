package controller

import (
	"go-pagination/model"
	"net/http"
	"path/filepath"
	"strconv"
	"text/template"
)

type ReturnData struct {
	Pagination Pagination
	Accounts   []model.Account
}

func HandleIndex() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var returnData ReturnData

		filepath := filepath.Join("view", "index.html")
		tmpl := template.Must(template.ParseFiles(filepath))

		page, _ := strconv.Atoi(r.URL.Query().Get("page"))
		count, _ := strconv.Atoi(r.URL.Query().Get("count"))

		if page == 0 {
			page = 1
		}

		if count < 10 {
			count = 10
		}

		returnData.Pagination = CalculatePagination(count, page)
		returnData.Accounts = model.ReadAccounts(returnData.Pagination.FirstData, count)

		// fmt.Println(returnData)
		tmpl.Execute(w, returnData)
	}
}
