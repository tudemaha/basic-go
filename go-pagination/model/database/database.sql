CREATE DATABASE IF NOT EXISTS sipomas_test;

USE sipomas_test;

CREATE TABLE IF NOT EXISTS accounts (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(15) NOT NULL,
    email varchar(100) NOT NULL,
    role varchar(10) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY username (username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;