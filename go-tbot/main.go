package main

import (
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	_ "github.com/joho/godotenv/autoload"
)

func main() {
	userOne := 991467950
	userTwo := 1211176874

	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_APITOKEN"))
	if err != nil {
		panic(err)
	}

	bot.Debug = true

	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 30

	updates := bot.GetUpdatesChan(updateConfig)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		var msg tgbotapi.MessageConfig
		if update.Message.Chat.ID == int64(userOne) {
			msg = tgbotapi.NewMessage(int64(userTwo), update.Message.Text)
		} else if update.Message.Chat.ID == int64(userTwo) {
			msg = tgbotapi.NewMessage(int64(userOne), update.Message.Text)
		} else {
			msg = tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
		}

		if _, err := bot.Send(msg); err != nil {
			panic(err)
		}

	}
}
