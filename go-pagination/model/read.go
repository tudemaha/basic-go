package model

import "log"

type Account struct {
	ID       int
	Username string
	Email    string
	Role     string
}

func GetRowsNum() int {
	var count int
	db := Connection()
	defer db.Close()

	row := db.QueryRow("SELECT COUNT(*) FROM accounts")
	err := row.Scan(&count)
	if err != nil {
		log.Fatalf("GetRowsNum fatal error: %v", err)
	}

	return count
}

func ReadAccounts(start int, limit int) []Account {
	db := Connection()
	defer db.Close()

	var result []Account

	rows, err := db.Query("SELECT id, username, email, role FROM accounts LIMIT ?, ?", start, limit)
	if err != nil {
		log.Fatalf("ReadTable fatal error: %v", err)
	}

	for rows.Next() {
		var each Account
		err := rows.Scan(&each.ID, &each.Username, &each.Email, &each.Role)
		if err != nil {
			log.Fatalf("rows scan fatal error: %v", err)
		}
		result = append(result, each)
	}

	return result
}
