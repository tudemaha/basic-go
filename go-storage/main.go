package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

func main() {

	// startServer()

	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile("cirhss-dictionary-9020a75074cf.json"))
	if err != nil {
		fmt.Println(err.Error())
	}

	bkt := client.Bucket("enggano-input")

	obj := bkt.Object("images/Pamphlet.png")
	r, err := obj.NewReader(ctx)
	if err != nil {
		log.Println(err)
	}
	defer r.Close()

	data, _ := io.ReadAll(r)
	os.WriteFile("Pamphlet.png", data, 0755)

	// file, err := os.Open("kamen.jpg")
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// defer file.Close()

	// w := bkt.Object("images/kamen.jpg").NewWriter(ctx)
	// defer w.Close()

	// io.Copy(w, file)

	// fmt.Println("sukses bos")

	// url, err := bkt.SignedURL("data", &storage.SignedURLOptions{
	// 	GoogleAccessID: "cirhss-local-dev@cirhss-dictionary.iam.gserviceaccount.com",
	// 	Method:         "POST",
	// 	Expires:        time.Now().Add(5 * time.Minute),
	// })
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(url)
}
