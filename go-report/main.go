package main

import (
	"log"
	"path"
	"time"

	"github.com/google/uuid"
	"github.com/jung-kurt/gofpdf"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

type Input struct {
	Name         string
	Position     string
	Status       string
	StartPeriode time.Time
	EndPeriode   time.Time
	BasicSalary  float64
	Bonus        float64
	PaidLeave    float64
	Permission   float64
	Insurance    float64
	TotalA       float64
	TotalB       float64
	Total        float64
	GenerateDate time.Time
}

func main() {
	p := message.NewPrinter(language.Indonesian)
	employee := Input{
		Name:         "Mahardika",
		Position:     "Back End",
		Status:       "Part Time",
		StartPeriode: time.Now(),
		EndPeriode:   time.Now(),
		BasicSalary:  1000000,
		Bonus:        500000,
		PaidLeave:    10000,
		Permission:   0,
		Insurance:    20000,
		TotalA:       1500000,
		TotalB:       10,
		Total:        1470000,
		GenerateDate: time.Now(),
	}

	pdf := gofpdf.New("P", "mm", "A5", "")
	// header
	pdf.SetHeaderFunc(func() {
		pdf.ImageOptions("./avatar.png", 45, 6, 60, 0, false, gofpdf.ImageOptions{}, 0, "")
		width, _ := pdf.GetPageSize()
		pdf.Line(0, 25, width, 25)
	})
	// footer
	pdf.SetFooterFunc(func() {
		pdf.SetFont("Arial", "", 8)
		_, height := pdf.GetPageSize()
		pdf.Text(10, height-10, employee.GenerateDate.Format("Monday, 02 January 2006 15:04:05"))
	})

	pdf.AddPage()

	pdf.SetFont("Arial", "B", 16)
	pdf.Text(55.8, 34, "SALARY SLIP")

	pdf.SetFont("Arial", "", 12)
	pdf.Text(20, 44, "Name")
	pdf.Text(50, 44, ":")
	pdf.Text(54, 44, employee.Name)

	pdf.Text(20, 50, "Position")
	pdf.Text(50, 50, ":")
	pdf.Text(54, 50, employee.Position)

	pdf.Text(20, 56, "Status")
	pdf.Text(50, 56, ":")
	pdf.Text(54, 56, employee.Status)

	pdf.Text(20, 66, "Start Period")
	pdf.Text(50, 66, ":")
	pdf.Text(54, 66, employee.StartPeriode.Format("Monday, 02 January 2006"))

	pdf.Text(20, 72, "End Period")
	pdf.Text(50, 72, ":")
	pdf.Text(54, 72, employee.EndPeriode.Format("Monday, 02 January 2006"))

	pdf.Text(20, 82, "Basic Salary")
	pdf.Text(50, 82, ":")
	pdf.Text(54, 82, p.Sprintf("Rp%.2f", employee.BasicSalary))

	pdf.Text(20, 88, "Bonus")
	pdf.Text(50, 88, ":")
	pdf.Text(54, 88, p.Sprintf("Rp%.2f", employee.Bonus))

	pdf.Text(20, 94, "Total A")
	pdf.Text(50, 94, ":")
	pdf.Text(54, 94, p.Sprintf("Rp%.2f", employee.TotalA))

	pdf.Text(20, 104, "Paid Leave")
	pdf.Text(50, 104, ":")
	pdf.Text(54, 104, p.Sprintf("Rp%.2f", employee.PaidLeave))

	pdf.Text(20, 110, "Permission")
	pdf.Text(50, 110, ":")
	pdf.Text(54, 110, p.Sprintf("Rp%.2f", employee.Permission))

	pdf.Text(20, 116, "Insurance")
	pdf.Text(50, 116, ":")
	pdf.Text(54, 116, p.Sprintf("Rp%.2f", employee.Insurance))

	pdf.Text(20, 122, "Total B")
	pdf.Text(50, 122, ":")
	pdf.Text(54, 122, p.Sprintf("Rp%.2f", employee.TotalB))

	pdf.SetFont("Arial", "BU", 12)
	pdf.Text(20, 134, "Total")
	pdf.Text(54, 134, p.Sprintf("Rp%.2f", employee.Total))

	pdf.SetFont("Arial", "B", 12)
	pdf.Text(50, 134, ":")

	filePath := path.Join("slips", uuid.New().String()+".pdf")
	err := pdf.OutputFileAndClose(filePath)
	if err != nil {
		log.Fatalf("fatal cuy, %v", err)
	}
}
