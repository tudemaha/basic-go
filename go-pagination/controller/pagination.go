package controller

import (
	"math"

	"go-pagination/model"
)

type Pagination struct {
	FirstData   int
	Pages       []int
	CurrentPage int
	LastPage    int
}

func CalculatePagination(perPage int, currentPage int) Pagination {
	var pagination Pagination

	dataCount := model.GetRowsNum()
	pageCount := int(math.Ceil(float64(dataCount) / float64(perPage)))

	pagination.FirstData = (currentPage * perPage) - perPage
	pagination.CurrentPage = currentPage
	pagination.Pages = make([]int, pageCount)

	for i := 0; i < pageCount; i++ {
		pagination.Pages[i] = i + 1
	}

	pagination.LastPage = pageCount

	return pagination
}
