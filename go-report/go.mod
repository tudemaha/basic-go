module go-report

go 1.19

require (
	github.com/jung-kurt/gofpdf v1.16.2
	golang.org/x/text v0.3.0
)

require github.com/google/uuid v1.3.0
