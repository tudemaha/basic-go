package main

import (
	"log"

	"github.com/gin-gonic/gin"
)

func startServer() {
	router := gin.Default()
	router.MaxMultipartMemory = 8 << 20
	router.POST("/upload", func(ctx *gin.Context) {
		// single file
		// file, err := ctx.FormFile("file")
		// if err != nil {
		// 	ctx.AbortWithStatusJSON(400, err)
		// }
		// log.Println(file.Filename)
		// ctx.SaveUploadedFile(file, file.Filename)

		// ctx.JSON(200, file.Filename)

		// multiple file
		form, _ := ctx.MultipartForm()
		files := form.File["files"]
		ctx.JSON(200, files)
	})

	router.Run(":8080")
}

func Test() {
	log.Print("test")
}
