package main

import (
	"fmt"
	"unicode"
)

func main() {
	// testing
	balineseUnicode := '\u1b33'
	balineseAscii := 6963
	balineseLetter := 'ᬳ'

	fmt.Println(rune(balineseUnicode) == rune(balineseAscii))
	fmt.Println(rune(balineseUnicode) == rune(balineseLetter))
	fmt.Println(rune(balineseUnicode) == rune(balineseLetter))

	// unicode can be concatenated
	test := string(balineseUnicode) + string(rune(7006))
	fmt.Println(test)

	// a char of a unicode encoded string isn't in unicode
	word := "ᬲᬸᬳᬭ᭄᭟"
	fmt.Println(rune(word[0]))

	// but you can use iteration to get the unicode and decode it to ascii
	for _, c := range word {
		fmt.Println(c, "or", string(c))
	}

	// checking unicode range
	fmt.Println(unicode.In(rune(balineseAscii), unicode.Balinese))
}
