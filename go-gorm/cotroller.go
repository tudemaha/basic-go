package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type UserInput struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

func OpenSQL() *gorm.DB {
	dsn := "root@tcp(127.0.0.1:3306)/gorm_test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalf("InitialMigration fatal error: %v", err)
	}

	return db
}

func GetAllUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "GET" {
			db := OpenSQL()

			var users []User
			db.Find(&users)
			json.NewEncoder(w).Encode(users)
		}
	}
}

func NewUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			db := OpenSQL()

			defer r.Body.Close()
			b, _ := io.ReadAll(r.Body)

			var data UserInput
			err := json.Unmarshal(b, &data)
			if err != nil {
				log.Fatalf("Umarshal fatal error: %v", err)
			}

			db.Create(&User{Name: data.Name, Email: data.Email})
			json.NewEncoder(w).Encode(string("oke"))
		}
	}
}

func DeleteUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "DELETE" {
			db := OpenSQL()

			path := r.URL.Path
			parts := strings.Split(path, "/")
			email := parts[len(parts)-1]

			var user User
			db.Where("email = ?", email).Find(&user)
			db.Delete(&user)
			json.NewEncoder(w).Encode(string("oke"))
		}

		if r.Method == "PUT" {
			db := OpenSQL()

			path := r.URL.Path
			parts := strings.Split(path, "/")
			email := parts[len(parts)-1]

			defer r.Body.Close()
			b, _ := io.ReadAll(r.Body)

			var data UserInput
			err := json.Unmarshal(b, &data)
			if err != nil {
				log.Fatalf("Umarshal fatal error: %v", err)
			}

			var user User
			db.Where("email = ?", email).Find(&user)
			user.Name = data.Name

			db.Save(&user)
			json.NewEncoder(w).Encode(string("oke"))
		}
	}
}
