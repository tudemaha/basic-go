package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"strconv"
)

type StudentData struct {
	ID      int
	Name    string
	Address string
}

func ReadLineCsv(c *csv.Reader) {
	for {
		rec, err := c.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%+v\n", rec)
	}
}

func ReadAllCsv(c *csv.Reader) {
	data, err := c.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	studentList := ParseReadAll(data)
	fmt.Printf("%+v\n", studentList)
}

func ParseReadAll(data [][]string) []StudentData {
	var studentData []StudentData

	for _, line := range data {
		var rec StudentData
		for i, field := range line {
			if i == 0 {
				rec.ID, _ = strconv.Atoi(field)
			} else if i == 1 {
				rec.Name = field
			} else if i == 2 {
				rec.Address = field
			}
		}
		studentData = append(studentData, rec)
	}
	return studentData
}